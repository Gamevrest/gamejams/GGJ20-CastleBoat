﻿using System;
using System.Collections.Generic;
using Entities;

namespace SaveSystem
{
    [Serializable]
    public class SaveData
    {
        public List<ResourceAmount> resources = new List<ResourceAmount>();
    }
}