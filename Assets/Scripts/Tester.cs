﻿using EventsSystem;
using EventsSystem.Payloads;
using GameManagerSystem;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class Tester : MonoBehaviour
{
    public string eventToTest1;
    public string eventToTest2;

    private GameEventListener _evt;

    private void OnEnable()
    {
       _evt =  GameManager.EventsManager.RegisterEvent(eventToTest2, PrintPayload);
    }

    private void OnDisable()
    {
        _evt.UnregisterSelf();
    }

    public void PrintPayload(Payload payload)
    {
        print($"received : [{payload}] of type [{payload.GetType()}]");
        switch (payload)
        {
            case IntPayload intPayload:
                print($"int = [{intPayload.Data}]");
                break;
            case StringPayload stringPayload:
                print($"string = [{stringPayload.Data}]");
                break;
            default:
                print($"Empty Payload");
                break;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Tester))]
public class SaveTesterEditor : Editor
{
    private string _stringTest;
    private int _intTest;

    public override void OnInspectorGUI()
    {
        GUILayout.Label("General", EditorStyles.boldLabel);
        base.OnInspectorGUI();
        var targ = target as Tester;
        if (targ == null)
            return;
        EditorGUILayout.Separator();
        GUILayout.Label("Data tests", EditorStyles.boldLabel);
        using (new GUILayout.HorizontalScope())
        {
            if (GUILayout.Button("save data"))
                GameManager.Instance.SaveData();
            if (GUILayout.Button("load data"))
                GameManager.Instance.LoadData();
        }

        EditorGUILayout.Separator();
        GUILayout.Label("Events tests", EditorStyles.boldLabel);
        if (GUILayout.Button("test empty payload"))
        {
            GameManager.EventsManager.Raise(targ.eventToTest1);
            GameManager.EventsManager.Raise(targ.eventToTest2);
        }
        
        using (new GUILayout.HorizontalScope())
        {
            _stringTest = EditorGUILayout.TextField(_stringTest);
            if (GUILayout.Button("test string payload"))
            {
                GameManager.EventsManager.Raise(targ.eventToTest1, _stringTest);
                GameManager.EventsManager.Raise(targ.eventToTest2, _stringTest);
            }
        }

        using (new GUILayout.HorizontalScope())
        {
            _intTest = EditorGUILayout.IntField(_intTest);
            if (GUILayout.Button("test int payload"))
            {
                GameManager.EventsManager.Raise(targ.eventToTest1, _intTest);
                GameManager.EventsManager.Raise(targ.eventToTest2, _intTest);
            }
        }
    }
}
#endif