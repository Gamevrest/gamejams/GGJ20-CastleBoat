﻿using System;

namespace Entities
{
    [Serializable]
    public class ResourceAmount
    {
        public Resource resource;
        public int amount;
    }
}