﻿using System;
using System.Collections;
using GameManagerSystem;
using TreeSystem;
using UnityEngine;
using Random = System.Random;
using Tree = TreeSystem.Tree;

namespace Entities
{
    public enum AiType
    {
        Throne,
        Resource
    };

    public class Enemy : MovingEntity
    {
        public int overrideFirstNode = -1;
        public AiType aiType;
        private Tree _tree;

        public bool isAttacking;
        public int atkPower = 10;
        public Entity attackTarget;

        public float attackCoolDown = 1;
        private bool _onCoolDown;

        public Enemy(int life, AiType aiType)
        {
            curLife = maxLife = life;
            this.aiType = aiType;
        }

        private new void Start()
        {
            base.Start();
            _tree = GameManager.Instance.tree;
            var rnd = new Random((int) DateTime.Now.Ticks);
            var entryNb = overrideFirstNode > -1 ? overrideFirstNode : rnd.Next(_tree.entries.Count);
            //Debug.Log($"random between 0 and {_tree.entries.Count} -> {entryNb}");
            intermediateNode = _tree.entries[entryNb]; //determine where to start from
            targetNode = intermediateNode; // un peu en dur pour le faire aller a son entry
            //Debug.Log($"{name} is starting and will go to entry {targetNode.Name}");
            shouldMove = true;
        }

        protected override void OnArriveToTarget()
        {
//            Debug.Log($"{name} has arrived to {targetNode.Name}");
            if (targetNode.IsThrone)
            {
                //Debug.Log($"{name} has arrived to THE THRONE");
                shouldMove = false;
                isAttacking = true;
                attackTarget = targetNode.Entity;
            }
            else
            {
                SetNewTarget(DetermineFinalTarget(), targetNode);
                //Debug.Log($"{name} has arrived to an entry and will continue to {targetNode.Name}");
            }
        }

        protected override void OnArriveToIntermediate()
        {
            if (!intermediateNode.IsOpen)
            {
                shouldMove = false;
                isAttacking = true;
                attackTarget = intermediateNode.Entity;
            }
        }

        private new void Update()
        {
            base.Update();
            if (isAttacking && attackTarget != null)
            {
                if (!_onCoolDown)
                {
                    attackTarget.Attack(atkPower);
                    StartCoroutine(CoolDown());
                }
                if (attackTarget.IsDead())
                {
                    shouldMove = true;
                    attackTarget = null;
                }
            }
        }

        private IEnumerator CoolDown()
        {
            _onCoolDown = true;
            yield return new WaitForSeconds(attackCoolDown);
            _onCoolDown = false;
        }

        private Node DetermineFinalTarget()
        {
            switch (aiType)
            {
                case AiType.Throne:
                    return GameManager.Instance.tree.throneRoom;
                case AiType.Resource:
                    return GameManager.Instance.tree.throneRoom; //todo set to resource
                default:
                    return GameManager.Instance.tree.throneRoom;
            }
        }
        
        protected override void TriggerDeath()
        {
            Destroy(gameObject);
        }
        
        // Node GetFirstNode(Node targetNode) // pour trouver l'entrée la plus opti
        // {
        //     if (GameManager.Instance.tree.entries.Count == 0)
        //         return null;
        //     Node returnNode = GameManager.Instance.tree.entries[0];
        //     NodeScored bestNode = GetClosestNodeTo(GameManager.Instance.tree.entries[0], targetNode);
        //     foreach (var entryNode in GameManager.Instance.tree.entries)
        //     {
        //         NodeScored closestNodeTo = GetClosestNodeTo(entryNode, targetNode);
        //         if (closestNodeTo.Score < bestNode.Score)
        //         {
        //             returnNode = entryNode;
        //             bestNode = closestNodeTo;
        //         }
        //     }
        //
        //     return returnNode;
        // }
        //
        // NodeScored
        //     GetClosestNodeTo(Node sourceNode,
        //         Node targetNode) // pour trouver le prochain node le plus proche a partir du node actuel
        // {
        //     if (sourceNode.Neighbors.Count == 0)
        //         return null;
        //     NodeScored bestNode = new NodeScored(sourceNode.Neighbors[0], 999);
        //     foreach (var linkedNode in sourceNode.Neighbors)
        //     {
        //         if (linkedNode == null)
        //             continue;
        //         var oldNodeList = new List<Node>();
        //         oldNodeList.Add(sourceNode);
        //         int currentScore = GetNodeValue(linkedNode, targetNode, oldNodeList, 0);
        //         if (currentScore < bestNode.Score)
        //         {
        //             bestNode.Score = currentScore;
        //             bestNode.Node = linkedNode;
        //         }
        //
        //         print(linkedNode.entity.name + " : " + currentScore);
        //     }
        //
        //     return bestNode;
        // }
        //
        // int GetNodeValue(Node self, Node targetNode, List<Node> oldNodeList,
        //     int depth) // pour calculer la profondeur de chaque node
        // {
        //     if (self == targetNode)
        //         return 0;
        //     if (!oldNodeList.Contains(self))
        //         oldNodeList.Add(self);
        //
        //     int def = 1000;
        //     int tmp = 0;
        //     foreach (var linkedNode in self.Neighbors)
        //     {
        //         if (linkedNode == null)
        //             continue;
        //         if (!oldNodeList.Contains(linkedNode))
        //         {
        //             List<Node> newOldList = new List<Node>(oldNodeList);
        //             tmp = GetNodeValue(linkedNode, targetNode, newOldList, depth++);
        //             if (tmp < def)
        //                 def = tmp;
        //         }
        //     }
        //
        //     return def;
        // }

       
    }
}