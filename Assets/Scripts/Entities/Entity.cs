﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Entities
{
    public abstract class Entity : MonoBehaviour
    {
        public Image healthBar;
        public int curLife;
        public int maxLife;

        protected void LoseLife(int amount)
        {
            curLife -= amount;
            if (curLife <= 0)
                TriggerDeath();
        }

        private void GainLife(int amount)
        {
            curLife = Mathf.Min(curLife + amount, maxLife);
        }

        protected void GainFullLife()
        {
            GainLife(maxLife);
        }

        protected void Update()
        {
            if (healthBar != null)
                healthBar.fillAmount = Mathf.Max(0, (float) curLife / (float) maxLife);
        }

        protected abstract void TriggerDeath();

        public void Attack(int atkPower)
        {
            curLife -= atkPower;
            if (curLife <= 0)
                TriggerDeath();
        }

        public bool IsDead()
        {
            return curLife <= 0;
        }
    }
}