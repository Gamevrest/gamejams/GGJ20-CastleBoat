﻿using System.Collections.Generic;
using GameManagerSystem;
using TreeSystem;
using UnityEngine;
using Utils;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Entities
{
    public abstract class Repairable : Entity
    {
        public Sprite broken;
        public Sprite notBroken;

        public SpriteRenderer renderer;
        public List<ResourceAmount> cost;
        [HideInInspector] public Node node;
        public List<Repairable> neighbors = new List<Repairable>();
        protected GameManager GameManager;

        protected Repairable()
        {
            node = new Node("Repairable", this);
        }

        private void Awake()
        {
            // todo ehance pour protect un peu
            foreach (var neighbor in neighbors)
            {
                if (neighbor == null)
                    continue;
                node.Neighbors.Add(neighbor.GetComponent<Repairable>().node);
            }
        }

        protected virtual void Start()
        {
            curLife = maxLife;
            GameManager = GameManager.Instance;
        }

        public void TryRepair()
        {
            if (curLife == maxLife) return;
            if (!GameManager.HasEnoughResources(cost)) return;
            GainFullLife();
            if (renderer && notBroken != null)
                renderer.sprite = notBroken;
            GameManager.UseResources(cost);
            node.IsOpen = false;
        }

        protected override void TriggerDeath()
        {
            node.IsOpen = true;
            if (renderer && broken != null)
                renderer.sprite = broken;
        }

        public void AddNeighbor(Repairable neighbor)
        {
            if (neighbors.Contains(neighbor))
                return;
            neighbors.Add(neighbor);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            foreach (var neighbor in neighbors)
                if (neighbor != null)
                    Gizmos.DrawLine(transform.position, neighbor.transform.position);
        }

        public void LinkNeighbours()
        {
            foreach (var neighbor in neighbors)
                if (neighbor != null)
                    neighbor.AddNeighbor(this);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Repairable))]
    public class RepairableEditor : Editor
    {
        public static void DrawNodeArray(Repairable targ)
        {
            CustomEditorUtils.DrawHeader("Tools");
            if (GUILayout.Button("link neighbors"))
                targ.LinkNeighbours();
        }
    }
#endif
}