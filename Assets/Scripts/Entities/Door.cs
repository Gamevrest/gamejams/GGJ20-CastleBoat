﻿using GameManagerSystem;
using Utils;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Entities
{
    public class Door : Repairable
    {
        public bool isEntryDoor;
        private new void Start()
        {
            base.Start();
            node.Name = $"Door {name}";
            // if (content.resource != null)
            //     node.Name += $" ({content.resource.name})";
            node.IsEntrance = isEntryDoor;
            if (isEntryDoor)
            {
                node.IsOpen = true;
                GameManager.Instance.tree.entries.Add(node);
            }
        }
    }
    
#if UNITY_EDITOR
    [CustomEditor(typeof(Door))]
    public class DoorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            CustomEditorUtils.DrawHeader("General");
            base.OnInspectorGUI();

            var targ = target as Door;
            if (targ == null)
                return;
            RepairableEditor.DrawNodeArray(targ);
        }
    }
#endif
}