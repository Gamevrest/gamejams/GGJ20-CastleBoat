﻿using System.Collections;
using UnityEngine;
using Utils;
#if UNITY_EDITOR
using UnityEditor;

#endif
namespace Entities
{
    public class SpawnRoom : Room
    {
        public GameObject soldatPrefab;
        public Transform container;
        private int _i = 0;

        protected override IEnumerator Generator()
        {
            while (gameObject.activeSelf)
            {
                if (resourceGeneratedOrNeeded.resource != null && shouldGenerate &&
                    GameManager.HasEnoughResource(resourceGeneratedOrNeeded))
                {
                    GameManager.UseResource(resourceGeneratedOrNeeded);
                    var obj = Instantiate(soldatPrefab, container);
                    obj.name = $"Soldier {_i++}";
                    var transform1 = container;
                    obj.transform.position = transform1.position;
                    obj.transform.rotation = transform1.rotation;
                    var script = obj.GetComponent<Soldat>();
                    script.startRoom = this;
                }

                yield return new WaitForSeconds(timeToGenerate);
            }

            yield return null;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Room))]
    public class SpawnRoomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            CustomEditorUtils.DrawHeader("General");
            base.OnInspectorGUI();

            var targ = target as Room;
            if (targ == null)
                return;
            RepairableEditor.DrawNodeArray(targ);
        }
    }
#endif
}