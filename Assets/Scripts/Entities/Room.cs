﻿using System.Collections;
using System.Collections.Generic;
using GameManagerSystem;
using UnityEngine;
using Utils;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Entities
{
    public class Room : Repairable
    {
        public bool isThroneRoom;
        public ResourceAmount resourceGeneratedOrNeeded;
        public float timeToGenerate = 10;
        public List<Transform> slots = new List<Transform>();
        public bool shouldGenerate;

        private new void Start()
        {
            base.Start();
            node.Name = $"Room {name}";
            node.IsThrone = isThroneRoom;
            if (isThroneRoom)
                GameManager.Instance.tree.throneRoom = node;
            foreach (Transform child in transform)
                slots.Add(child);
            shouldGenerate = true;
            GameManager = GameManager.Instance;
            StartCoroutine(Generator());
        }

        private new void Update()
        {
            base.Update();
            if (isThroneRoom && curLife <= 0)
                GameManager.Instance.EndGame(false);
        }

        protected virtual IEnumerator Generator()
        {
            while (gameObject.activeSelf)
            {
                if (resourceGeneratedOrNeeded.resource != null && shouldGenerate)
                    GameManager.AddResource(resourceGeneratedOrNeeded);
                yield return new WaitForSeconds(timeToGenerate);
            }
            yield return null;
        }

        public void SetDestination()
        {
            GameManager.SelectRoomToGo(this);
        }
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(Room))]
    public class RoomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            CustomEditorUtils.DrawHeader("General");
            base.OnInspectorGUI();

            var targ = target as Room;
            if (targ == null)
                return;
            RepairableEditor.DrawNodeArray(targ);
        }
    }
#endif
}