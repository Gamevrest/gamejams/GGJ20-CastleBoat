﻿using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
#endif
using TreeSystem;
using UnityEngine;

namespace Entities
{
    public abstract class MovingEntity : Entity
    {
        public enum Type
        {
            Enemy,
            Soldat
        }

        public Type type;
        public GameObject displayer;
        protected Animator _animator;
        public float speed = 3;
        public Node intermediateNode;
        public Node targetNode;
        public bool shouldMove;

        private List<Node> _prevPath;

        protected void Start()
        {
            _prevPath = new List<Node>();
            intermediateNode = null;
            targetNode = null;
            _animator = displayer.GetComponent<Animator>();
        }

        protected void SetNewTarget(Node target, Node actual)
        {
            targetNode = target;
            _prevPath.Clear();
            ChooseNextIntermediate(actual);
        }

        protected void Move()
        {
            if (targetNode == null || intermediateNode == null)
            {
                Debug.Log("Not enough info to continue");
                return;
            }

            if (Vector2.Distance(transform.position, targetNode.Entity.transform.position) < 1f)
            {
                targetNode.AddEntityInRoom(this);
                //Debug.Log($"Arrived to target {targetNode.Name}");
                _prevPath.Clear();
                OnArriveToTarget();
                if (_animator != null)
                    _animator.Play("Move");
                return;
            }

            if (Vector2.Distance(transform.position, intermediateNode.Entity.transform.position) < 1f)
            {
                intermediateNode.AddEntityInRoom(this);
                //Debug.Log($"Arrived to intermediate {intermediateNode.Name}");
                OnArriveToIntermediate();
                ChooseNextIntermediate(intermediateNode);
                if (_animator != null)
                    _animator.Play("Move");
                return;
            }

            intermediateNode.RemoveEntityInRoom(this);
            targetNode.RemoveEntityInRoom(this);
            var destination = intermediateNode.Entity.transform.position;
            if (displayer != null)
                displayer.transform.right = destination - displayer.transform.position;

            var position = transform.position;
            position = Vector2.MoveTowards(new Vector2(position.x, position.y), destination, speed * Time.deltaTime);
            transform.position = position;
        }

        protected abstract void OnArriveToIntermediate();
        protected abstract void OnArriveToTarget();

        private void ChooseNextIntermediate(Node startNode)
        {
            if (startNode.Neighbors.Count <= 0)
            {
                //Debug.Log($"Isolated room, should not happen {startNode.Name}");
                return;
            }

            foreach (var neighbor in startNode.Neighbors)
            {
                if (neighbor == targetNode)
                {
                    _prevPath.Add(startNode);
                    intermediateNode = neighbor;
                    return;
                }

                if (!_prevPath.Contains(neighbor))
                {
                    _prevPath.Add(startNode);
                    intermediateNode = neighbor;
                    //Debug.Log($"choose intermediate {intermediateNode.Name}");
                    return;
                }
            }

            var prevNode = _prevPath.Last();
            _prevPath.Remove(prevNode);
            _prevPath = _prevPath.Prepend(startNode).ToList();
            intermediateNode = prevNode;
            //Debug.Log($"{name} has reached a dead end on {startNode.Name} backtracking {prevNode.Name}");
        }

        protected new void Update()
        {
            base.Update();
            if (shouldMove)
                Move();
        }
    }
}