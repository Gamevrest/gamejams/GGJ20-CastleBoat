﻿using UnityEngine;

namespace Entities
{
    [UnityEngine.CreateAssetMenu(fileName = "New resource", menuName = "Scriptables/New Resource")]
    public class Resource : UnityEngine.ScriptableObject
    {
        
        public new string name;
        public Sprite sprite;
    }
}