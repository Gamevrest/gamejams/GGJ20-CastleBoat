﻿using GameManagerSystem;
using UnityEngine;
using Utils;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Entities
{
    public class Soldat : MovingEntity
    {
        public int atkPower = 5;
        public Resource resourceToHeal;
        public float healPriceMult = 1;
        public Room targetRoom;
        public Room startRoom;
        public GameManager gameManager;
        public bool isAttacking;
        public MovingEntity attackTarget = null;

        protected new virtual void Start()
        {
            base.Start();
            gameManager = GameManager.Instance;
            transform.position = startRoom.transform.position;
            SetDestination(startRoom);
        }

        public void SetDestination(Room destination)
        {
            targetRoom = destination;
            targetNode = targetRoom.node;
            intermediateNode = startRoom.node;
            shouldMove = true;
            isAttacking = false;
            attackTarget = null;
        }

        public void TryHeal()
        {
            var amountToHeal = (int) ((maxLife - curLife) * healPriceMult);
            if (!gameManager.HasEnoughResource(resourceToHeal, maxLife - curLife)) return;
            GainFullLife();
            gameManager.UseResource(resourceToHeal, amountToHeal);
        }

        public void SelectSelfToMove()
        {
            gameManager.selectedSoldat = this;
        }

        protected override void TriggerDeath()
        {
            shouldMove = false;
        }

        protected override void OnArriveToIntermediate()
        {
        }

        protected override void OnArriveToTarget()
        {
            shouldMove = false;
            if (_animator != null)
                _animator.Play("Idle");
            ChooseEnemy();
        }

        public void ChooseEnemy()
        {
            if (targetNode.EntitiesInRoom.Count > 0)
            {
                foreach (var entity in targetNode.EntitiesInRoom)
                {
                    if (entity == attackTarget)
                        return;
                    if (entity.type == Type.Enemy)
                    {
                        attackTarget = entity;
                        isAttacking = true;
                        shouldMove = false;
                        return;
                    }
                }
            }

            isAttacking = false;
            attackTarget = null;
        }

        private new void Update()
        {
            base.Update();
            if (!shouldMove)
                ChooseEnemy();
            if (isAttacking && attackTarget != null)
            {
                ChooseEnemy();
                attackTarget.Attack(atkPower);
            }
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Soldat))]
    public class SoldatEditor : Editor
    {
        private Room _debugTarget;

        public override void OnInspectorGUI()
        {
            CustomEditorUtils.DrawHeader("General");
            base.OnInspectorGUI();
            var targ = target as Soldat;
            if (targ == null) return;
            CustomEditorUtils.DrawHeader("Debug");
            _debugTarget = (Room) EditorGUILayout.ObjectField("Debug Target", _debugTarget, typeof(Room), true);
            if (GUILayout.Button("set target"))
                targ.SetDestination(_debugTarget);
        }
    }
#endif
}