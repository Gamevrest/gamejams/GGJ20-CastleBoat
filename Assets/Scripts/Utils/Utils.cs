﻿using UnityEngine;

namespace Utils
{
    public class Tools
    {
        public static float RoundFloat(float nbToRound, int decimalsToKeep = 1)
        {
            var div = Mathf.Pow(10, decimalsToKeep);
            return (int) (nbToRound * div) / div;
        }

        public static void CleanChildren(Transform parent)
        {
            foreach (Transform child in parent)
                Object.Destroy(child.gameObject);
        }

        public static void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
    Application.Quit();
#endif
        }
    }
}