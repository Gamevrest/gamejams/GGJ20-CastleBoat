﻿using System;
using System.Collections.Generic;

namespace TreeSystem
{
    [Serializable]
    public class Tree
    {
        public List<Node> entries = new List<Node>();
        public Node throneRoom;
    }
}