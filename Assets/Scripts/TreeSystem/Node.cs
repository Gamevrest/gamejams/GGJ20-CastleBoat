﻿using System.Collections.Generic;
using Entities;


namespace TreeSystem
{
    public class Node
    {
        public string Name;
        public readonly List<Node> Neighbors = new List<Node>();
        public bool IsOpen;
        public bool IsThrone;
        public bool IsEntrance;
        public readonly Repairable Entity;

        public readonly List<MovingEntity> EntitiesInRoom;

        public Node(string name, Repairable entity)
        {
            Entity = entity;
            Name = name;
            EntitiesInRoom = new List<MovingEntity>();
        }

        public void AddEntityInRoom(MovingEntity e)
        {
            if (EntitiesInRoom.Contains(e))
                return;
            EntitiesInRoom.Add(e);
        }

        public void RemoveEntityInRoom(MovingEntity e)
        {
            if (EntitiesInRoom.Contains(e))
                EntitiesInRoom.Remove(e);
        }
    }
}