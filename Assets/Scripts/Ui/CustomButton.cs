﻿using UnityEngine;
using UnityEngine.Events;

namespace Ui
{
    public class CustomButton : MonoBehaviour
    {
        [Header("basic Settings")] 
        public SpriteRenderer targetImage;

        [Header("Callback events")] public UnityEvent highlight;
        public UnityEvent unHighlight;
        public UnityEvent onSelect;

        public void Awake()
        {
            if (targetImage == null)
                targetImage = GetComponent<SpriteRenderer>();
        }

        public void ForceState(bool isHighlighted)
        {
            if (isHighlighted)
                highlight.Invoke();
            else
                unHighlight.Invoke();
        }

        public void Highlight()
        {
            highlight.Invoke();
        }

        public void UnHighlight()
        {
            unHighlight.Invoke();
        }

        public void Select()
        {
            onSelect.Invoke();
        }
    }
}