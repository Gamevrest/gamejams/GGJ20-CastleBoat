﻿using UnityEngine;

namespace Ui
{
    public class MouseRaycast : MonoBehaviour
    {
        public LayerMask layersToCheck;
        public float distance = 20;
        private Camera _mainCamera;
        private CustomButton _curButton;

        private void Start()
        {
            _mainCamera = Camera.main;
        }

        private void Update()
        {
            var ray = Physics2D.Raycast(_mainCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, distance,
                layersToCheck);
            if (_curButton)
            {
                _curButton.UnHighlight();
                _curButton = null;
            }
            if (ray.collider == null)
                return;
            var newButton = ray.transform.GetComponent<CustomButton>();
            if (_curButton != null)
                _curButton.UnHighlight();
            if (newButton == null)
                return;
            _curButton = newButton;
            if (Input.GetMouseButtonUp(0))
                _curButton.Select();
            else
                _curButton.Highlight();
        }
    }
}