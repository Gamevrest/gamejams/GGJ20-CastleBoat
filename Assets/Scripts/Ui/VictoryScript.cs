﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VictoryScript : MonoBehaviour
{
    public Button mainMenu, nextLevel;
    public GameObject victory;
    float time = 0;
    public float _size = 0.5f;

    public float _upSizeTime = 1.0f;

    void Update()
    {
        if (time <= _upSizeTime)
        {
            victory.gameObject.transform.localScale = Vector3.one * (1 + _size * time);
        }
        else if (time <= _upSizeTime * 2)
        {
            victory.gameObject.transform.localScale = Vector3.one * (2 * _size * _upSizeTime + 1 - time * _size);
            
        }
        else
        {
            victory.gameObject.transform.localScale = Vector3.one;
            resetAnim();
        }
        time += Time.deltaTime;
        
    }

    public void resetAnim()
    {
        time = 0;
    }

    public void mainClick()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void nextLevelClick()
    {
        SceneManager.LoadScene("Level" + (PlayerPrefs.GetInt("level") + 1));
    }

    public void replay()
    {
        SceneManager.LoadScene("Level" + PlayerPrefs.GetInt("level"));
    }
}
