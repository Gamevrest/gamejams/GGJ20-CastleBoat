﻿using UnityEngine;

public class ColliderHighlighter : MonoBehaviour
{
    private EdgeCollider2D _collider;
    private LineRenderer _lineRenderer;
    public Color color = Color.red;
    public Material material;
    public float thickness = .2f;

    protected void Start()
    {
        _collider = GetComponent<EdgeCollider2D>();
        _lineRenderer = gameObject.AddComponent<LineRenderer>();
        CreateHighlight();
        StopHighlight();
    }

    public void StartHighlight()
    {
        _lineRenderer.enabled = true;
    }

    public void StopHighlight()
    {
        _lineRenderer.enabled = false;
    }

    private void CreateHighlight()
    {
        if (_collider == null || _lineRenderer == null)
            return;
        _lineRenderer.material = material;
        const float zPos = -1;
        var pColliderPos = (_collider as EdgeCollider2D)?.points;
        _lineRenderer.startColor = color;
        _lineRenderer.endColor = color;
        _lineRenderer.startWidth = thickness;
        _lineRenderer.endWidth = thickness;
        for (var i = 0; i < pColliderPos.Length; i++)
            pColliderPos[i] = _collider.transform.TransformPoint(pColliderPos[i]);
        _lineRenderer.positionCount = pColliderPos.Length + 1;
        for (var i = 0; i < pColliderPos.Length; i++)
        {
            Vector3 finalLine = pColliderPos[i];
            finalLine.z = zPos;
            _lineRenderer.SetPosition(i, finalLine);
            if (i == (pColliderPos.Length - 1))
            {
                finalLine = pColliderPos[0];
                finalLine.z = zPos;
                _lineRenderer.SetPosition(pColliderPos.Length, finalLine);
            }
        }
    }
}