﻿using Entities;
using GameManagerSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResourceAmountFetcher : MonoBehaviour
{
    public Resource resourceToFetch;
    public TextMeshProUGUI text;
    public Image icon;

    private void Start()
    {
        UpdateText();
        if (resourceToFetch.sprite != null)
            icon.sprite = resourceToFetch.sprite;
    }

    private void UpdateText()
    {
        if (text != null)
            text.text = GameManager.Instance.FetchResourceAmount(resourceToFetch).ToString();
    }

    private void Update()
    {
        UpdateText();
    }
}