﻿using UnityEngine;
using TMPro;

public class ResourceDisplay : MonoBehaviour
{
    public int plank, brick, ration, weapon;
    public TextMeshProUGUI plankDisplayer, brickDisplayer, rationDisplayer, weaponDisplayer;

    void Update()
    {
        plankDisplayer.text = plank.ToString();
        brickDisplayer.text = brick.ToString();
        rationDisplayer.text = ration.ToString();
        weaponDisplayer.text = weapon.ToString();
    }
}
