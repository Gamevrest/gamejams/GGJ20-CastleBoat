﻿using GameManagerSystem;
using TMPro;
using UnityEngine;

public class EnemiesManager : MonoBehaviour
{
    public TextMeshProUGUI wave;
    public GameObject[] bounds; //spawn points on map, 0 is up right then 1 bot right etc
    public GameObject[] enemies;
    public GameObject enemiesGameObject;
    private GameManager game;
    public bool finished;
    private Wave currentWave;
    private float time = -1f;
    private int waveCounter = -1;
    private int subWaveIterator;
    private float spawnSpeed = 0.4f;
    private int nbWave = -1;
    private int level; // -1 is endless, 0 tutorial, more is associated level
    private Levels levels;
    private Level currentLevel;
    private int randRead;
    //private bool instantiated = false;
    public int monsterNb;
    
    void Start()
    {
        spawnSpeed = 1f;//PlayerPrefs.GetFloat("spawnSpeed");
        //level = PlayerPrefs.GetInt("level");
        level = 1;
        //enemiesGameObject = GameObject.Find("Enemies");
        game = GameManager.Instance;
        levels = new Levels();
        currentLevel = levels.GetLevel(level);
        LoadNextWave();
        nbWave = currentLevel.GetLength();
        UpdateCounter();
    }

    void Update()
    {
        if (game.isFinished)
            return;
        if (finished)
        {
            if (enemiesGameObject.transform.childCount == 0)
                game.EndGame(true);
            return;
        }
        time += Time.deltaTime * spawnSpeed;
        if ((currentWave.subWaves[subWaveIterator].time >= 0 && time > currentWave.subWaves[subWaveIterator].time) 
            || enemiesGameObject.transform.childCount == 0) //is subwave time ended or is there no enemy left
            LoadNextSubwave();

        if (subWaveIterator >= currentWave.subWaves.Length) // if wave finished
            LoadNextWave();
    }

    void LoadNextSubwave()
    {
        UpdateCounter();
        SpawnWave(currentWave.subWaves[subWaveIterator]);
        time = 0;
        subWaveIterator++;
    }
    
    void LoadNextWave()
    {
        if (level != 0)
            randRead = Random.Range(0, 4); //for the wave to spawn from any border order ("rotating" all wave spawning)
        subWaveIterator = 0;
        time = -6f;
        if (++waveCounter >= nbWave && nbWave > 0)
        {
            time = 0f;
            finished = true;
        }
        else
        {
            if (currentLevel != null)
            {
                currentWave = currentLevel.GetWave(waveCounter);
                if (currentWave.generateNew)
                    currentWave = WavePicker(currentWave.difficulty);
            }
            else
                currentWave = WavePicker(GetDifficulty());
        }
        if (level == -1)
            spawnSpeed += 0.05f;
    }

    void UpdateCounter()
    {
        wave.text = "Wave " + (waveCounter + 1);
        if (nbWave > 0)
            wave.text += "/" + nbWave;
    }
    
    GameObject GetEnemyFromString(string name)
    {
        foreach (var enemy in enemies)
        {
            if (enemy.name == name || enemy.name[0].ToString() == name)
                return enemy;
        }
        if (name != "")
            return enemies[0];
        return null;
    }

    readonly float[] difficultyChances = {50, 25, 17.5f, 7.5f}; // % chances per difficulty (0, 1, 2 ,3)

    int GetDifficulty()
    {
        int rand = Random.Range(0, 101) +
                   waveCounter * 5; //from wave 20 and beyond, 100% chance to have max difficulty
        for (int i = difficultyChances.Length - 1; i > 0; i--)
        {
            if (rand > 100 - difficultyChances[i])
                return i;
        }
        return 0;
    }

    Wave WavePicker(int difficulty)
    {
        int rand = Random.Range(0, allWaves[difficulty].Length);
        return allWaves[difficulty][rand];
    }

    void SpawnWave(SubWave wave)
    {
        for (int i = 0; i < 4; i++)
        {
            GameObject inst = GetEnemyFromString(wave.enemies[(i + randRead) % 4]);
            if (inst)
            {
                var dif = bounds[i].transform.position.x < 0 ? 0.8f : -0.8f;
                Vector3 pos = bounds[i].transform.position +
                              new Vector3(Random.Range(-1.2f + dif, 1.2f + dif), Random.Range(-0.5f, 0.5f), 0);
               var obj =  Instantiate(inst, pos, bounds[i].transform.rotation, enemiesGameObject.transform);
               obj.name = $"mob {monsterNb++}";
            }
        }
    }
    
    private readonly Wave[][] allWaves
        = // Wave[difficulty][waveIndex]      //map : wave((time before this wave) "0","1","2","3") (b : barrier)
        {
                                         //          3   0
            new[]                        //            b
            {                            //          2   1
                // difficulty 0                     			
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "", "", ""}), //progressive
                    new SubWave(1f, new[] {"", "D", "", ""}),
                    new SubWave(1f, new[] {"", "", "D", ""}),
                    new SubWave(1f, new[] {"", "", "G", "D"}),
                    new SubWave(1.5f, new[] {"G", "", "D", ""}),
                    new SubWave(1.5f, new[] {"", "D", "", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"", "D", "", "D"}), //spam all
                    new SubWave(1.5f, new[] {"", "D", "", "D"}),
                    new SubWave(1.5f, new[] {"G", "", "D", ""}),
                    new SubWave(1.5f, new[] {"D", "", "D", ""})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "D", "", ""}), //around
                    new SubWave(1.5f, new[] {"", "D", "D", ""}),
                    new SubWave(1.5f, new[] {"", "", "G", "D"}),
                    new SubWave(1.5f, new[] {"D", "", "", "D"}),
                    new SubWave(3f, new[] {"D", "D", "D", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(2f, new[] {"D", "D", "D", "D"}), //hard start    
                    new SubWave(3.5f, new[] {"", "D", "", "D"}),
                    new SubWave(1.5f, new[] {"G", "", "G", ""}),
                    new SubWave(1.5f, new[] {"", "G", "", "D"}),
                    new SubWave(1.5f, new[] {"D", "", "D", ""}),
                    new SubWave(1.5f, new[] {"", "D", "", "D"})
                })
            },
            new[]
            {
                // difficulty 1
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "", "", ""}), //spam one side
                    new SubWave(1f, new[] {"", "", "", "D"}),
                    new SubWave(1f, new[] {"D", "", "", "D"}),
                    new SubWave(1f, new[] {"D", "", "", "W"}),
                    new SubWave(1f, new[] {"D", "", "", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"", "D", "", ""}), //spam right
                    new SubWave(1f, new[] {"", "", "D", ""}),
                    new SubWave(1f, new[] {"", "D", "D", ""}),
                    new SubWave(1f, new[] {"", "W", "D", ""}),
                    new SubWave(1f, new[] {"", "D", "D", ""})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "", "", ""}), //spam up
                    new SubWave(1f, new[] {"", "D", "", ""}),
                    new SubWave(1f, new[] {"D", "D", "", ""}),
                    new SubWave(1f, new[] {"W", "D", "", ""}),
                    new SubWave(1f, new[] {"D", "D", "", ""})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"", "", "D", ""}), //spam down
                    new SubWave(1f, new[] {"", "", "", "D"}),
                    new SubWave(1f, new[] {"", "", "D", "D"}),
                    new SubWave(1f, new[] {"", "", "W", "D"}),
                    new SubWave(1f, new[] {"", "", "D", "D"})
                })
            },
            new[]
            {
                // difficulty 2
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "D", "", ""}), //progressive 2
                    new SubWave(1f, new[] {"", "D", "D", ""}),
                    new SubWave(1f, new[] {"", "", "D", "D"}),
                    new SubWave(1f, new[] {"D", "", "", "D"}),
                    new SubWave(1.5f, new[] {"D", "W", "D", "W"})
                }),
                new Wave(new[]
                {
                    new SubWave(0.5f, new[] {"D", "", "", ""}), //spam all 2
                    new SubWave(0.5f, new[] {"D", "", "", ""}),
                    new SubWave(0.5f, new[] {"", "D", "", ""}),
                    new SubWave(0.5f, new[] {"", "D", "", ""}),
                    new SubWave(0.5f, new[] {"", "", "D", ""}),
                    new SubWave(0.5f, new[] {"", "", "D", ""}),
                    new SubWave(0.5f, new[] {"", "", "", "D"}),
                    new SubWave(0.5f, new[] {"", "", "", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"", "", "", "D"}), //extend
                    new SubWave(1f, new[] {"", "", "D", "D"}),
                    new SubWave(1f, new[] {"", "D", "", "D"}),
                    new SubWave(1f, new[] {"D", "", "", "D"}),
                    new SubWave(1f, new[] {"", "D", "D", "D"}),
                    new SubWave(1f, new[] {"D", "D", "", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(2f, new[] {"D", "W", "D", "W"}), //hard start 2
                    new SubWave(2f, new[] {"", "D", "", "D"}),
                    new SubWave(1f, new[] {"D", "", "D", ""}),
                    new SubWave(1f, new[] {"D", "", "D", ""}),
                    new SubWave(1f, new[] {"", "D", "", "D"})
                })
            },
            new[]
            {
                // difficulty 3
                new Wave(new[]
                {
                    new SubWave(3f, new[] {"W", "D", "W", "D"}), //regular
                    new SubWave(2f, new[] {"D", "W", "D", "W"}),
                    new SubWave(2f, new[] {"D", "D", "D", "D"}),
                    new SubWave(2f, new[] {"D", "D", "D", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "", "", ""}), //again ?
                    new SubWave(0.2f, new[] {"D", "", "", ""}),
                    new SubWave(1f, new[] {"", "D", "", ""}),
                    new SubWave(0.2f, new[] {"D", "", "", ""}),
                    new SubWave(0.2f, new[] {"D", "", "", ""}),
                    new SubWave(1f, new[] {"", "", "D", ""}),
                    new SubWave(0.2f, new[] {"D", "", "", ""}),
                    new SubWave(0.2f, new[] {"C", "", "", ""}),
                    new SubWave(1f, new[] {"", "", "", "D"}),
                    new SubWave(0.2f, new[] {"D", "", "", ""}),
                    new SubWave(0.2f, new[] {"D", "", "", ""})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "", "", ""}), //speeding up
                    new SubWave(1f, new[] {"", "D", "", ""}),
                    new SubWave(1f, new[] {"", "", "D", ""}),
                    new SubWave(1f, new[] {"", "", "", "D"}),
                    new SubWave(0.6f, new[] {"D", "", "", ""}),
                    new SubWave(0.6f, new[] {"", "D", "", ""}),
                    new SubWave(0.6f, new[] {"", "", "D", ""}),
                    new SubWave(0.6f, new[] {"", "", "", "D"}),
                    new SubWave(0.3f, new[] {"D", "", "", ""}),
                    new SubWave(0.3f, new[] {"", "D", "", ""}),
                    new SubWave(0.3f, new[] {"", "", "D", ""}),
                    new SubWave(0.3f, new[] {"", "", "", "D"})
                }),
                new Wave(new[]
                {
                    new SubWave(1f, new[] {"D", "", "", ""}), //2 by 2
                    new SubWave(0.2f, new[] {"D", "", "", ""}),
                    new SubWave(0.6f, new[] {"", "D", "", ""}),
                    new SubWave(0.2f, new[] {"", "D", "", ""}),
                    new SubWave(0.6f, new[] {"", "", "D", ""}),
                    new SubWave(0.2f, new[] {"", "", "D", ""}),
                    new SubWave(0.6f, new[] {"", "", "", "D"}),
                    new SubWave(0.2f, new[] {"", "", "", "D"}),
                    new SubWave(0.6f, new[] {"D", "", "D", ""}),
                    new SubWave(0.2f, new[] {"D", "", "D", ""}),
                    new SubWave(0.6f, new[] {"", "D", "", "D"}),
                    new SubWave(0.2f, new[] {"", "D", "", "D"})
                })
            }
        };
}
