﻿using System.Collections.Generic;

public class SubWave
{
    public readonly float time;
    public readonly string[] enemies;

    public SubWave(float time, string[] enemies)
    {
        this.time = time;
        this.enemies = enemies;
    }
}

public class Wave
{
    public readonly SubWave[] subWaves;
    public readonly bool generateNew;
    public readonly int difficulty;

    public Wave(SubWave[] subWaves)
    {
        this.subWaves = subWaves;
        generateNew = false;
        difficulty = -1;
    }

    public Wave(int difficulty)
    {
        subWaves = null;
        generateNew = true;
        this.difficulty = difficulty;
    }
}

public class Levels
{
    private readonly List<Level> levels;

    public Levels()
    {
        levels = new List<Level>
        {
            new Tutorial(),
            new Level1(),
            new Level2(),
            new Level3(),
            new Level4(),
            new Level5()
        };
    }

    public Level GetLevel(int id) // -1 is endless, 0 tutorial, more is associated level
    {
        if (id < 0 || id > levels.Count)
            return null;
        return levels[id];
    }
}

public abstract class Level
{
    protected Wave[] waves;

    public Wave GetWave(int id)
    {
        return waves[id];
    }

    public int GetLength()
    {
        return waves.Length;
    }
}
public class Tutorial : Level    //Subwave (0f, timeBefore this wave, if -1, wait for the previous to finish, new []{enemy 1, 2, 3, 4);
{
    public Tutorial()
    {
        waves = new[]
        {
            new Wave(new[]
            {
                new SubWave(0f, new[] {"D", "", "G", ""}), //introducing 1 diablotin, waiting for player to find out how to kill it with prism
                new SubWave(-1f, new[] {"", "D", "", ""}), //introducing 3 diablotin to show player can do AOE damage
                new SubWave(0.5f, new[] {"", "D", "", ""}),
                new SubWave(0.5f, new[] {"", "D", "", ""})
            }),
            new Wave(new[]
            {
                new SubWave(-1f, new[] {"", "", "", ""}), //introducing 3 diablotin killable without doing anything
                new SubWave(1f, new[] {"D", "", "G", ""}),
                new SubWave(1f, new[] {"", "D", "", "D"}),
                new SubWave(4f, new[] {"", "", "D", ""}), //introducing 1 diablotin waiting for player to find out how to kill it moving heroe
                new SubWave(-1f, new[] {"", "D", "", ""}),
                new SubWave(0.5f, new[] {"G", "D", "G", ""}),
                new SubWave(0.5f, new[] {"G", "D", "", ""}) //spamming more to suggest him use 2 heroes
            }),
            new Wave(new[]
            {
                new SubWave(-1f, new[] {"G", "", "", ""}), // a real battle to practice
                new SubWave(2f, new[] {"G", "D", "", ""}), //2 on each side for challenge
                new SubWave(1f, new[] {"G", "D", "", ""}),
                new SubWave(3f, new[] {"", "", "D", ""}), 
                new SubWave(1f, new[] {"", "", "D", ""}),
                new SubWave(3f, new[] {"", "", "", "D"}),
                new SubWave(1f, new[] {"", "", "", "D"}),
                new SubWave(3f, new[] {"D", "G", "", ""}), 
                new SubWave(1f, new[] {"D", "", "", ""}),
                new SubWave(6f, new[] {"D", "D", "D", "D"}), //last wave to optimize AEO damage and manage all sides
                new SubWave(1.5f, new[] {"", "D", "G", ""}),
                new SubWave(1.5f, new[] {"", "D", "G", ""})
            })
        };
    }
}

public class Level1 : Level
{
    public Level1()
    {
        waves = new[]
        {
            new Wave(new[]
            {
                new SubWave(2f, new[] {"D", "", "", ""}), //slowly introducing diablotin, and spamming a little bit
                new SubWave(3f, new[] {"", "D", "", ""}),
                new SubWave(2f, new[] {"", "", "G", ""}),
                new SubWave(1f, new[] {"", "", "", "D"})
            }),
            new Wave(new[]
            {
                new SubWave(3f, new[] {"D", "", "", ""}), //progressive
                new SubWave(1f, new[] {"", "D", "", ""}),
                new SubWave(1f, new[] {"", "", "G", ""}),
                new SubWave(1f, new[] {"", "", "", "D"}),
                new SubWave(1.5f, new[] {"D", "", "D", ""}),
                new SubWave(1.5f, new[] {"", "G", "", "D"})
            }),
            new Wave(new[]
            {
                new SubWave(2f, new[] {"D", "D", "", ""}), //around
                new SubWave(1.5f, new[] {"", "G", "D", ""}),
                new SubWave(1.5f, new[] {"", "", "G", "D"}),
                new SubWave(1.5f, new[] {"G", "", "", "D"}),
                new SubWave(3f, new[] {"D", "G", "D", "G"})
            })
        };
    }
}

public class Level2 : Level
{
    public Level2()
    {
        waves = new[]
        {
            new Wave(new[]
            {
                new SubWave(2f, new[] {"D", "D", "", "D"}),  //introducing wizard
                new SubWave(3f, new[] {"", "", "G", ""}),
            }),
            new Wave(new[]
            {
                new SubWave(3f, new[] {"D", "", "G", ""}),    //repos
                new SubWave(0.5f, new[] {"", "D", "", "D"}),
            }),
            new Wave(0),
            new Wave(0)
        };
    }
}

public class Level3 : Level
{
    public Level3()
    {
        waves = new[]
        {
            new Wave(new[]
            {
                new SubWave(1.5f, new[] {"D", "", "", ""}),    //hello wizards
                new SubWave(1.5f, new[] {"", "D", "", ""}),
                new SubWave(1.5f, new[] {"W", "", "D", ""}),
                new SubWave(1.5f, new[] {"", "W", "", "D"})
            }),
            new Wave(0),
            new Wave(0),
            new Wave(new[]
            {
                new SubWave(1.5f, new[] {"W", "", "", ""}),    //hello wizards 2
                new SubWave(1.5f, new[] {"", "W", "", ""}),
                new SubWave(1.5f, new[] {"", "", "W", ""}),
                new SubWave(1.5f, new[] {"", "", "", "W"})
            }),
        };
    }
}

public class Level4 : Level
{
    public Level4()
    {
        waves = new[]
        {
            new Wave(new[]
            {
                new SubWave(1f, new[] {"D", "", "D", "D"}),    //introducing cerberus
                new SubWave(3f, new[] {"", "C", "", ""}),
            }),            
            new Wave(new[]
            {
                new SubWave(3f, new[] {"D", "", "", "D"}),    //repos
                new SubWave(1f, new[] {"", "D", "D", ""}),
            }),
            new Wave(0),
            new Wave(1),
            new Wave(1)
        };
    }
}

public class Level5 : Level
{
    public Level5()
    {
        waves = new[]
        {
            new Wave(1),
            new Wave(1),
            new Wave(2),
            new Wave(2),
            new Wave(2)
        };
    }
}