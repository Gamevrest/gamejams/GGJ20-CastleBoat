﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    [CreateAssetMenu(menuName = "Scriptables/Game Event Manager")]
    public class EventsManager : ScriptableObject
    {
        public List<GameEvent> events = new List<GameEvent>();

        // public void AddEvent(GameEvent @event)
        // {
        //     if (events.Contains(@event))
        //     {
        //         Debug.Log($"Event [{@event}] wasn't added because it already exists");
        //         return;
        //     }
        //
        //     events.Add(@event);
        //     Debug.Log($"Event [{@event}] was added to list");
        // }

        public GameEventListener RegisterEvent(string id, UnityAction<Payload> callback)
        {
            var evt = GetEvent(id);
            return evt == null ? null : evt.Register(new GameEventListener(evt, new PayloadUnityEvent(callback)));
        }

        public GameEvent GetEvent(string id)
        {
            foreach (var @event in events)
                if (@event.name == id)
                    return @event;

            Debug.LogWarning($"Couldn't find event [{id}]");
            return null;
        }

        public void Raise(string id, Payload p) => GetEvent(id).Raise(p);

        //Shortcuts for easy use
        public void Raise(string id) => GetEvent(id).Raise();
        public void Raise(string id, string s) => GetEvent(id).Raise(s);
        public void Raise(string id, int i) => GetEvent(id).Raise(i);
        public void Raise(string id, float f) => GetEvent(id).Raise(f);
    }
}