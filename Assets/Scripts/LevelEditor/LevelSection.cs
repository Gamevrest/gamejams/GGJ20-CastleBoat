﻿using System.Collections.Generic;
using UnityEngine;
using Utils;
using UnityEngine.UI;
using TMPro;

public class LevelSection : MonoBehaviour
{
    public SceneReference level1Scene;
    public SceneReference level2Scene;
    public SceneReference level3Scene;
    public SceneReference level4Scene;
    public SceneReference level5Scene;

    public void Level1() => level1Scene.Load();
    public void Level2() => level2Scene.Load();
    public void Level3() => level3Scene.Load();
    public void Level4() => level4Scene.Load();
    public void Level5() => level5Scene.Load();

    public List<GameObject> ButtonList = new List<GameObject>();
    public int currentLevel;

    public TextMeshProUGUI levelDisplayer;

    void Start()
    {
        // 현재 레벨이 3일경우 1, 2까지 언락
        int i;
        for (i = 0; i < currentLevel; i++)
        {
            ButtonList[i].transform.Find("Image").gameObject.SetActive(false); // 자물쇠 이미지 없앰
            ButtonList[i].transform.GetComponent<Button>().interactable = true;
        }
    }

    void Update()
    {
        levelDisplayer.text = "YOUR CURRENT LEVEL IS  " + currentLevel.ToString();
    }
}
