﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace DefaultNamespace
{
    [Serializable]
    [CreateAssetMenu(fileName = "New Rule Tile", menuName = "Tiles/Prefab Tile")]
    public class PrefabTile : Tile
    {
        
    }
}