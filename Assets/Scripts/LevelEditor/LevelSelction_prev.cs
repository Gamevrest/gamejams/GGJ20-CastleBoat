﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]

public class LevelSelction_prev : MonoBehaviour
{
    public int currentLevel, nextLevel;
    public TextMeshProUGUI levelDisplayer;

    public GameObject levelButtonPrefab;
    public GameObject levelGrid;

    public Text btnText;
    public Button level2, tmp; 

    public List<GameObject> ButtonList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        //currentLevel = 1;
        nextLevel = currentLevel + 1;
        int i;
        if(currentLevel>2) {
            level2.transform.Find("Image").gameObject.SetActive(false);
            level2.interactable = true;

            for (i=0; i < currentLevel-2; i++) {
                addLevelBtn(i+1);
            }

            for(i=0; i < currentLevel-1; i++)
            {
                ButtonList[i].transform.Find("Image").gameObject.SetActive(false);
                tmp = ButtonList[i].transform.GetComponent<Button>();
                tmp.interactable = true;
                tmp.onClick.AddListener(btnClick);
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        levelDisplayer.text = "YOUR CURRENT LEVEL IS  " + currentLevel.ToString();

        if (nextLevel == currentLevel)  //Level Up
        {
            addLevelBtn(currentLevel);
            nextLevel += 1;
            
            if(currentLevel>=3) //It works from level 3
            {
                ButtonList[currentLevel - 3].transform.Find("Image").gameObject.SetActive(false); // 자물쇠 이미지 없앰
                tmp = ButtonList[currentLevel - 3].transform.GetComponent<Button>();
                tmp.interactable = true; // 활성화
                tmp.onClick.AddListener(btnClick); 
            }
            else
            {
                level2.transform.Find("Image").gameObject.SetActive(false);
                level2.interactable = true;
            }
        }
    }

    public void addLevelBtn(int level)
    {
        level++;
        GameObject levelBtn = (GameObject)Instantiate(levelButtonPrefab, new Vector2(100, 100), Quaternion.identity) as GameObject;
        levelBtn.transform.SetParent(levelGrid.transform);
        levelBtn.name = "Level" + level.ToString();
        TextMeshProUGUI txt = levelBtn.transform.Find("Text (TMP)").GetComponent<TextMeshProUGUI>();
        txt.text = levelBtn.ToString().Substring(0, levelBtn.ToString().IndexOf("("));

        ButtonList.Add(levelBtn);
        levelBtn = GameObject.Find("Level" + level.ToString());
    }

    public void btnClick()
    {
        SceneManager.LoadScene("TestScene");
    }
}
