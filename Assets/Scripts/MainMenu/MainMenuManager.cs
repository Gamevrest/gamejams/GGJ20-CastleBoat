﻿using UnityEngine;
using Utils;

namespace MainMenu
{
    public class MainMenuManager : MonoBehaviour
    {
        private void Start()
        {
            if (PlayerPrefs.GetInt("level") <= 0)
                PlayerPrefs.SetInt("level", 1);
        }

        [Header("General")] public GameObject creditsCanvas;
        [Header("Scenes")] public SceneReference newGameScene;
        public SceneReference continueScene;

        public void ShowCredits() => creditsCanvas.SetActive(true);
        public void HideCredits() => creditsCanvas.SetActive(false);
        public void NewGame() => newGameScene.Load();
        public void Continue() => continueScene.Load();
        public void Quit() => Tools.QuitGame();

    }
 }