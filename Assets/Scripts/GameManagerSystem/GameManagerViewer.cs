﻿using UnityEngine;
using Utils;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GameManagerSystem
{

    public class GameManagerViewer : MonoBehaviour
    {
        public GameManager gameManager = GameManager.Instance;
        public SceneReference quitScene;

        private void Awake()
        {
            gameManager.RuntimeInitialize();
        }

        public void Quit()
        {
            quitScene.Load();
        }
        
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(GameManagerViewer))]
    public class GameManagerViewerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GUILayout.Label("General", EditorStyles.boldLabel);
            base.OnInspectorGUI();
            var manager = GameManager.Instance;
            EditorGUILayout.Separator();
        }
    }
#endif
}