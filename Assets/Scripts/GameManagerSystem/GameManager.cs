﻿using System;
using System.Collections.Generic;
using Entities;
using EventsSystem;
using SaveSystem;
using Ui;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
using Tree = TreeSystem.Tree;

#if UNITY_EDITOR

#endif

namespace GameManagerSystem
{
    [Serializable]
    public class GameManager
    {
        public enum ChosenTool
        {
            Troop = 0,
            Heal = 1,
            Repair = 2
        }

        public static GameManager Instance { get; } = new GameManager();
        public static EventsManager EventsManager => Instance.eventsManager;
        public static SaveData GameData => Instance.gameData;

        public Vector2 cursorDecal;
        public Texture2D[] cursorIcons;
        private ChosenTool _chosenTool;
        private MouseRaycast _mouseRaycast;

        public SaveData gameData = new SaveData();
        public EventsManager eventsManager;
        public Tree tree = new Tree();

        public Soldat selectedSoldat = null;

        public bool isFinished = false;

        private GameManager()
        {
        }

        //Use this one
        static GameManager()
        {

        }

        public void RuntimeInitialize()
        {
            Instance.eventsManager = Resources.Load<EventsManager>("EventsManager");
            Instance.eventsManager.RegisterEvent("PickTroopTool", t => { PickToolCallback(ChosenTool.Troop); });
            Instance.eventsManager.RegisterEvent("PickHealTool", t => { PickToolCallback(ChosenTool.Heal); });
            Instance.eventsManager.RegisterEvent("PickRepairTool", t => { PickToolCallback(ChosenTool.Repair); });

            _mouseRaycast = Object.FindObjectOfType<MouseRaycast>();
            PickToolCallback(0);
        }

        private void PickToolCallback(ChosenTool tool)
        {
            Cursor.SetCursor(cursorIcons[(int) tool], cursorDecal, CursorMode.Auto);
            _chosenTool = tool;
            switch (_chosenTool)
            {
                case ChosenTool.Troop:
                    _mouseRaycast.layersToCheck = LayerMask.GetMask("SoldatMove", "MoveTo");
                    break;
                case ChosenTool.Heal:
                    _mouseRaycast.layersToCheck = LayerMask.GetMask("SoldatHeal");
                    break;
                case ChosenTool.Repair:
                    _mouseRaycast.layersToCheck = LayerMask.GetMask("Repair");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SaveData() => SaveSystem.SaveSystem.Save(gameData);

        public void LoadData()
        {
            gameData = SaveSystem.SaveSystem.Load();
        }

        public ResourceAmount GetResourceAmount(Resource toFetch)
        {
            foreach (var resourceAmount in gameData.resources)
                if (resourceAmount.resource == toFetch)
                    return resourceAmount;
            return null;
        }

        public bool HasEnoughResource(Resource resource, int amount)
        {
            var r = GetResourceAmount(resource);
            if (r == null)
                return false;
            return amount <= r.amount;
        }

        public bool HasEnoughResource(ResourceAmount resourceAmount)
        {
            return HasEnoughResource(resourceAmount.resource, resourceAmount.amount);
        }

        public bool HasEnoughResources(List<ResourceAmount> needed)
        {
            foreach (var need in needed)
                if (!HasEnoughResource(need))
                    return false;

            return true;
        }

        public void UseResources(List<ResourceAmount> needed)
        {
            foreach (var need in needed)
                UseResource(need.resource, need.amount);
        }

        public void UseResource(Resource resource, int amount)
        {
            var r = GetResourceAmount(resource);
            if (r == null) return;
            r.amount -= amount;
        }

        public void UseResource(ResourceAmount resourceAmount)
        {
            UseResource(resourceAmount.resource, resourceAmount.amount);
        }

        public int FetchResourceAmount(Resource resource)
        {
            var r = GetResourceAmount(resource);
            return r?.amount ?? 0;
        }

        public void AddResource(Resource resource, int amount)
        {
            var r = GetResourceAmount(resource);
            if (r == null) return;
            r.amount += amount;
        }

        public void AddResource(ResourceAmount amount)
        {
            AddResource(amount.resource, amount.amount);
        }

        public void SelectRoomToGo(Room dest)
        {
            if (selectedSoldat == null) return;
            selectedSoldat.SetDestination(dest);
            selectedSoldat = null;
        }

        public void EndGame(bool win)
        {
            if (win)
            {
                PlayerPrefs.SetInt("level", PlayerPrefs.GetInt("level") + 1);
                SceneManager.LoadScene("VictoryScene");
            }
            SceneManager.LoadScene("DefeatScene");
        }
    }
}